#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <windows.h>
void inti();
int writefile();
int keycheck();
void add();
void sub();
void update();
void deleteall();
void list();
void setting();
void search();
void trashcan();
char *filepath="accountbook.data";
char *trashpath="trash.data";
char password[65]="none",number[129],ch;
struct accountbook{char name[21];char ID[129];char key[129];};
struct accountbook A[500];
int accountsum=0,keysum=0,num,p;
int main()
{
    system("color 3e");
    inti();
    int flag=1,correct=1,err=0;
    if(keysum)
    {
        while(1)
        {
            correct=keycheck();
            if(correct)break;
            else err++;
            if(err>=5)printf("請稍後再試!\n");
            if(err>=5)Sleep(1700);
            if(err>=5)exit(1);
        }
    }
    while(flag&&correct)
    {

        printf("**********************\n");
        printf("****歡迎使用帳密本****\n");
        printf("**===1.新增帳戶=====**\n");
        printf("**===2.刪除帳戶=====**\n");
        printf("**===3.更改帳戶=====**\n");
        printf("**===4.查詢帳戶=====**\n");
        printf("**===5.設定=========**\n");
        printf("**===6.退出=========**\n");
        printf("**********************\n\n");
        printf("請選擇 1 ~ 6 來作業: ");
        scanf("%s",number);
        if(strcmp(number,"trash")==0)
        {
            FILE *fptr=fopen(trashpath,"r");
            if(fptr!=NULL)
            {
                while((ch=getc(fptr))!=EOF)
                    printf("%c",ch);
            }
            system("pause");
            system("cls");
        }
        num=atoi(number);
        if(num>=1&&num<=6)
        {
            if(num==1)add();
            else if(num==2)sub();
            else if(num==3)update();
            else if(num==4)search();
            else if(num==5)setting();
            else flag=0;
        }
        else system("cls");
    }
    return 0;
}
void add()
{
    printf("您選擇了 新增帳戶\n\n");
    printf("請輸入名稱(不含空格): ");
    scanf("%s",A[accountsum].name);
    printf("請輸入帳號(不含空格): ");
    scanf("%s",A[accountsum].ID);
    printf("請輸入密碼(不含空格): ");
    scanf("%s",A[accountsum].key);
    accountsum++;
    if(writefile())printf("已新增!\n");
    return;
}
void sub()
{
    int i,sure,tra;
    printf("您選擇了 刪除帳戶\n\n");
    if(accountsum==0)
    {
        printf("無帳戶資訊!\n");
        return;
    }
    printf("***清單如下***\n");
    printf("編號\t帳戶名稱\n\n");
    for(i=0;i<accountsum;i++)printf("%d\t%s\n",i+1,A[i].name);
    printf("%d\t全部\n",i+1);
    printf("\n請選擇預刪除的帳戶編號: ");
    scanf("%s",number);
    num=atoi(number);
    if(num==accountsum+1)
    {
        deleteall();
        return;
    }
    if(num<1||num>accountsum){printf("編號錯誤!\n");return;}
    printf("確定刪除 %s ?(0.取消  1.確認): ",A[num-1].name);
    scanf("%d",&sure);
    if(sure)
    {
        tra=num;
        trashcan(tra);
        for(i=num-1;(i+1)<accountsum;i++)A[i]=A[i+1];
        accountsum--;
        if(writefile())printf("已刪除!\n");
    }
    return;
}
void deleteall()
{
    int sure,tra;
    printf("確定刪除全部?(0.取消  1.確認):");
    scanf("%d",&sure);
    if(sure)
    {
        FILE *fptr=fopen(filepath,"w");
        if(fptr!=NULL)
        {
            tra=0;
            trashcan(tra);
            accountsum=0;
            fprintf(fptr,"%d\n",keysum);
            fprintf(fptr,"%s\n",password);
            fprintf(fptr,"%d\n",accountsum);
            fclose(fptr);
            printf("File update sucess!\n");
            printf("已刪除全部!\n");
        }
        else printf("刪除失敗...\n");
    }
    else printf("已取消...\n");
    return;
}
void update()
{
    int i,sure;
    system("cls");
    printf("您選擇了 更改帳戶\n");
    if(accountsum==0)
    {
        printf("無帳戶資訊!\n");
        return;
    }
    printf("***清單如下***\n");
    printf("編號\t帳戶名稱\n\n");
    for(i=0;i<accountsum;i++)printf("%d\t%s\n",i+1,A[i].name);
    printf("\n請選擇預更改的帳戶編號: ");
    scanf("%s",number);
    num=atoi(number);
    if(num>0&&num<=accountsum)
    {
        printf("您要更改 %s 帳戶(1.名稱  2.密碼  3.取消)\n",A[num-1].name);
        scanf("%d",&sure);
        if(sure>=1&&sure<=2)
        {
            if(sure==1)
            {
                printf("請輸入名稱(不含空格): ");
                scanf("%s",A[num-1].name);
            }
            else if(sure==2)
            {
                printf("請輸入密碼(不含空格): ");
                scanf("%s",A[num-1].key);
            }
            if(writefile())printf("已更改!\n");
        }
        else printf("已取消...\n");
    }
    return;
}
void search()
{
    int i;
    num=0;
    printf("您選擇了 查詢帳戶\n");
    printf("清單如下:\n");
    while(1)
    {
        if(accountsum==0)
        {
            printf("無帳戶資訊!\n");
            break;
        }
        printf("編號\t帳戶名稱\n");
        for(i=0;i<accountsum;i++)printf("%d\t%s\n",i+1,A[i].name);
        printf("%d\t列出所有帳戶\n",i+1);
        printf("\n請輸入查詢編號(0.返回): ");
        scanf("%s",number);
        num=atoi(number);
        if(num>=0&&num<=accountsum+1)
        {

            if(num==0)
            {
                system("cls");
                break;
            }
            else if(num==accountsum+1)list();
            else
            {
                printf("\n%s 帳密\n",A[num-1].name);
                printf("%s\n",A[num-1].ID);
                printf("%s\n\n",A[num-1].key);
            }
        }
        else printf("編號錯誤!\n");
        system("pause");
        system("cls");
    }
    return;
}
void list()
{
    int i;
    system("cls");
    if(accountsum==0)printf("無帳戶資訊!\n");
    else
    {
        printf("總和 = %d 個\n",accountsum);
        printf("****帳戶清單顯示如下****\n");
        for(i=0;i<accountsum;i++)
        {
            printf("------------%d-----------\n",i+1);
            printf("%s:\n",A[i].name);
            printf("%s\n",A[i].ID);
            printf("%s\n",A[i].key);
        }
    }
    printf("\n");
    return;
}
int keycheck()
{
    char pass[65];
    int p,yes;
    printf("請輸入密碼: ");
    for(p=0;p<65;p++)
    {
        pass[p]=getch();
        if(pass[p]==13)break;
        printf("*");
    }
    pass[p]=0;
    if(strcmp(pass,"exit")==0)exit(1);
    if(strcmp(pass,password)==0)
    {
        system("cls");
        yes=1;
    }
    else
    {
        printf("密碼錯誤!\n");
        yes=0;
    }
    return yes;
}
void setting()
{
    char pass[65],pass2[65],movepass[]="none";
    num=0;
    printf("您選擇了 設定\n\n");
    printf("1.設定密碼\n");
    printf("2.移除密碼\n");
    printf("3.返回主選單\n\n");
    printf("請選擇 1 ~ 3 來作業: ");
    while(num<1||num>3)
    {
        scanf("%s",number);
        num=atoi(number);
        if(num==1)
        {
            if(keysum)
            {
                printf("請輸入舊密碼: ");
                for(p=0;p<65;p++)
                {
                    pass[p]=getch();
                    if(pass[p]==13)break;
                    printf("*");
                }
                pass[p]=0;
                if(strcmp(pass,password)!=0)
                {
                    printf(" 密碼錯誤!\n");
                    return;
                }
            }
            printf("\n請輸入新密碼: ");
            for(p=0;p<65;p++)
            {
                pass[p]=getch();
                if(pass[p]==13)break;
                printf("*");
            }
            pass[p]=0;
            printf("\n請在一次輸入新密碼: ");
            for(p=0;p<65;p++)
            {
                pass2[p]=getch();
                if(pass2[p]==13)break;
                printf("*");
            }
            pass2[p]=0;
            if(strcmp(pass,pass2)==0)
            {
                strcpy(password,pass2);
                keysum=1;
                if(writefile())printf("\n密碼已設定!\n");
            }
            else
            {
                printf("\n密碼不一致!\n");
                printf("設定失敗...\n");
            }
        }
        else if(num==2)
        {
            if(keysum)
            {
                printf("請輸入密碼: ");
                for(p=0;p<65;p++)
                {
                    pass[p]=getch();
                    if(pass[p]==13)break;
                    printf("*");
                }
                pass[p]=0;
                if(strcmp(pass,password)!=0)
                {
                    printf(" 密碼錯誤!\n");
                    return;
                }
            }
            keysum=0;
            strcpy(password,movepass);
            if(writefile())printf("\n移除完成!\n");
            else printf("\n移除失敗!\n");
        }
        else return;
    }
    return;
}
void trashcan(int tra)
{
    int i;
    FILE *fptr=fopen(trashpath,"a");
    if(tra==0)
    {
        for(i=0;i<accountsum;i++)fprintf(fptr,"%s %s %s\n",A[i].name,A[i].ID,A[i].key);
    }
    else fprintf(fptr,"%s %s %s\n",A[tra-1].name,A[tra-1].ID,A[tra-1].key);
    fclose(fptr);
    return;
}
void inti()
{
    int i;
    FILE *fptr=fopen(filepath,"r");
    FILE *fptr2=fopen(trashpath,"a");
    if(fptr!=NULL)
    {
        printf("檔案載入中...\n");
        fscanf(fptr,"%d",&keysum);
        fscanf(fptr,"%s",password);
        fscanf(fptr,"%d",&accountsum);
        printf("帳戶總和 %d 個\n",accountsum);
        for(i=0;i<accountsum;i++)fscanf(fptr,"%s%s%s",A[i].name,A[i].ID,A[i].key);
        printf("載入完成!\n");
    }
    else
    {
        fptr=fopen(filepath,"w");
        printf("無找到檔案! 建立新檔案中...\n");
        keysum=accountsum=0;
        fprintf(fptr,"%d\n",keysum);
        fprintf(fptr,"%s\n",password);
        fprintf(fptr,"%d\n",accountsum);
        printf("建立完成!\n");
    }
    fclose(fptr);
    fclose(fptr2);
    return;
}
int writefile()
{
    int i,sucess;
    FILE *fptr=fopen(filepath,"w");
    if(fptr!=NULL)
    {
        fprintf(fptr,"%d\n",keysum);
        fprintf(fptr,"%s\n",password);
        fprintf(fptr,"%d\n",accountsum);
        for(i=0;i<accountsum;i++)fprintf(fptr,"%s %s %s\n",A[i].name,A[i].ID,A[i].key);
        fclose(fptr);
        sucess=1;
    }
    if (sucess)return 1;
    else return 0;
}
